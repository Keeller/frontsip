import React,{Component} from 'react';
import Headers from './../../Components/Header/header'
import Main from './../../Components/MainPage/main'
import {Route,Switch} from 'react-router-dom'
import Form from './../../Components/LoginForm/Login'
import Orders from './../../Components/orders/orderForm'
import Table from './../../Components/OrderTable/orderTable'
class Layout extends Component{
    constructor(props){
        super(props);
        this.state={
            name:"",
            lname:""
        }
    }
    loginHandler(name,lname,token){
        localStorage.setItem("token",token);
        this.setState({name:name,lname:lname});

    }
    render(){
        return(<div>
                <main>
                    <Headers name={this.state.name} lname={this.state.lname} />
                        <Switch>
                            <Route path="/" exact component={Main} />
                            <Route path="/login" exact component={Form} lhd={this.loginHandler.bind(this)}/>
                            <Route path="/order" component={Orders}/>
                            <Route path="/orderlist" component={Table}/>
                            
                        </Switch>
                </main>
            </div>);
    }
}

export default Layout;