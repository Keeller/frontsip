import React from 'react'
import Form from './../Components/LoginForm/Login'

const Container=class{
    constructor(props) {
        super(props);
        this.state={
            showModal:false   
        }
    }

    modalShowHandler(){
        this.setState({showModal:!this.state.showModal});
    }

    render(){
        return (
            <>
                {this.state.showModal&&<Form chd={this.modalShowHandler.bind(this)} />}
            </>
        );
    }
}