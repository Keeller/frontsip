//import './contacts.css'
import './main.css'
import React from 'react'
import {NavLink} from 'react-router-dom'
export default props=>(
	<nav>
		<div className="menu main">
			<h1>
			CHATAUQAUA
			</h1>
			<ul>
				<li>
					<NavLink to="/">ГЛАВНАЯ</NavLink>
				</li>
				<li>
					<NavLink to="#">О НАС</NavLink>
				</li>
				<li>
					<NavLink to="#">ЦЕНЫ</NavLink>
				</li>
				<li>
					<NavLink to="#">ОТЗЫВЫ</NavLink>
				</li>
				<li>
					<NavLink to="contacts.html">КОНТАКТЫ</NavLink>
				</li>
				<li>
					<NavLink to="/login">ВХОД</NavLink>
				</li>
				<li>
					{props.name}
				</li>
				<li>
					{props.lname}
				</li>
			</ul>	
		</div>
	</nav>	

)