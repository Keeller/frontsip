import React from 'react'
import axios from 'axios'
import './style.css'

const Form=class UserForm extends React.Component {
    constructor(props) {
      super(props);
      this.state = {name: "",
                    pass:"",
                    nv:true,
                    p:true
                    };
 
      this.onChangeN = this.onChangeN.bind(this);
      this.onChangeL = this.onChangeL.bind(this);
      this.handleSubmit = this.handleSubmit.bind(this);
    }
    validateN(name){
      //console.log(name.len);
      return name.length<255&&name.length>-1;
    }
    validateL(pass){
      return pass.length<255&&pass.length>-1;
    }
   
    onChangeN(e) {
      var val = e.target.value;
      var valid=this.validateN(val);
      valid?this.setState({name: val,nv:true}):this.setState({nv:false});
  }
  onChangeL(e) {
      var val = e.target.value;
      var valid=this.validateL(val);
      valid?this.setState({Lname: val,p:true}):this.setState({p:false});
  }
  
 
  validate(){
    return(this.state.p&&this.state.nv);
  }
    handleSubmit(e) {
      e.preventDefault();
      if(this.validate()){
          axios.post('/user/login',{
                name:this.state.name,
                pass:this.state.pass
          }).then(response=>{
              if(response.status=='OK'){
                    this.props.lhd(response.name,response.Lname,response.token);
                    this.props.history.push('/');
              }

          }).catch(err=>{
              console.log(err);
              this.props.history.push('/');
          });
        
        }
    }
 
    render() {
      return (

          <div className="container">
          <section id="content">
          <form onSubmit={this.handleSubmit}>
            
                  <label>Логин:</label><br />
                  <input type="text" value={this.state.name} onChange={this.onChangeN}/><br/>
                  <label>Пароль:</label><br />
                  <input type="text" value={this.state.pass} onChange={this.onChangeL}/><br />
              
              <input type="submit" value="Отправить" />
          </form>        
          </section>
          </div>
      );
    }
  }

  export default Form;