import React from 'react'
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
import axios from 'axios'
import Table from './../OrderTable/orderTable'

const Oform=class extends React.Component {
    constructor(props){
        super(props);
        this.state={
            options:[],
            currEl:""
        }
    }
    change(event){
        this.setState({currEl:event.target.value});
    }
    componentDidMount(){
        axios.get('/listofParsers').then(response=>{
            if(response.status=='OK'){
            this.setState({options:response.data.options,currEl:response.data.options[0]});
            }
        }).catch(err=>{
            console.log(err);
        });
    }
    sendFormHandler(){
        axios.post('/setOrder').then(response=>{
            if(response.status=='OK'){
                //какой-то осмысленный ответ
            }
        }).catch(err=>{
            console.log(err);
        })
    }

    render(){
        return(
            <div>
            <form onSubmit={this.sendFormHandler.bind(this)}>
                <label>url</label><br />
                <input style={{width:"100%"}} type="text"  /><br />
                <label>Описание контента</label><br />
                <textarea style={{width:"100%"}} /><br />
                <label>Список модулей</label><br />
                <Dropdown onChange={this.change.bind(this)}  options={this.state.options}/><br/>
                
                <input type="submit" value="Добавить" />
            </form>
            <hr/>
                <Table />
            </div>
        );
    }
}

export default Oform;