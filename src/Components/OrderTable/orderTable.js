import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import axios from 'axios';


/*
function createData(name, calories, fat, carbs, protein) {
  return { name, calories, fat, carbs, protein };
}

const rows = [
  createData('Frozen yoghurt', 159, 6.0, 24, 4.0),
  createData('Ice cream sandwich', 237, 9.0, 37, 4.3),
  createData('Eclair', 262, 16.0, 24, 6.0),
  createData('Cupcake', 305, 3.7, 67, 4.3),
  createData('Gingerbread', 356, 16.0, 49, 3.9),
];
*/

export default class DenseTable extends React.Component {

  

    constructor(props){
        super(props);
        this.state={rows:[{name:'v',time:"22.02.2019",status:'OK'},{name:"xx",time:"22.03.2019",status:"Ok"}]
        }
    }
    
  componentDidMount(){
    this.loadContent();
  }

  UNSAFE_componentDidUpdate(){
      this.loadContent();
  }
  

  loadContent(){
      
    axios.get('/orderlist?token='+localStorage.getItem("token")).then(response=>{
        if(response.status=='OK'){
            let result = [];
            
            for(let i in response.data)
                result.push(response.data[i]);
            this.setState({rows:result});
        }
    }).catch(err=>{
        console.log(err);
    });
  }

  render(){
const classes =   makeStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      overflowX: 'auto',
    },
    table: {
      minWidth: 650,
    },
  }); 
  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Table className={classes.table} size="small" aria-label="a dense table">
          <TableHead>
            <TableRow>
              <TableCell>Заказ</TableCell>
              <TableCell>Время парсинга</TableCell>
              <TableCell>Статус</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {this.state.rows.map(row => (
              <TableRow key={row.id}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell >{row.time}</TableCell>
                <TableCell >{row.status}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    </div>
  );
            }
}